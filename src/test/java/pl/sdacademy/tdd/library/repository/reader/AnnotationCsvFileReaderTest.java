package pl.sdacademy.tdd.library.repository.reader;

import org.junit.Test;
import pl.sdacademy.tdd.library.domain.User;

import java.util.List;

import static org.junit.Assert.*;

public class AnnotationCsvFileReaderTest {

    @Test
    public void testLoadUsers() {
        AnnotationCsvFileReader loader = new
                AnnotationCsvFileReader<User>("/data/users.csv");

        List<User> list = loader.readFile(User.class);

    }

}