package pl.sdacademy.tdd.library.logic;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.FileReader;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.startsWith;


public class BookCollectionImplTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();


    @Test
    public void testReadFile3() throws IOException {
        thrown.expect(IOException.class);
        thrown.expectMessage(startsWith("test.txt (No such file or directory)"));
        FileReader reader = new FileReader("test.txt");
        reader.read();
        reader.close();
    }



}