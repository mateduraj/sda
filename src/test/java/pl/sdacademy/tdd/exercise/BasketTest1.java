package pl.sdacademy.tdd.exercise;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;

/**
 * Uruchom test:
 * Ctrl+ Shift+ F10
 */
public class BasketTest1 {


    @Before
    public void init() {
        System.out.println("INIT TEST");
    }


    @Test
    public void hasIphoneProduct() {
        //GIVEN
        Basket basket = new Basket(Arrays.asList(new Item("IPHONE", 5, 55)));

        //WHEN
        boolean iphone = basket.has("IPHONE");

        //THEN
        assertFalse("Basket should contains iphone product", iphone);
    }

}
