package pl.sdacademy.tdd.exercise;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static junit.framework.TestCase.assertTrue;

/**
 * Uruchom test:
 * Ctrl+ Shift+ F10
 */
public class BasketTest3 {


    @Rule
    public ExpectedException exception = ExpectedException.none();


    @Before
    public void init() {
        System.out.println("INIT TEST");
    }


    @Test(expected = BasketAmountLessThanZeroException.class)
    public void whenSubtractWithExpectedUsage() {
        //GIVEN
        Basket basket = new Basket(Arrays.asList(new Item("VODKA", 5, 55)));
        basket.subtract(500);
    }


    @Test()
    public void whenSubtractWithRuleUsageExample() {
        exception.expect(BasketAmountLessThanZeroException.class);
        Basket basket = new Basket(Arrays.asList(new Item("VODKA", 5, 55)));
        basket.subtract(500);
    }

    /**
     *
     * @Rule - Mechanizm ten jest alternatywą dla @Before i @After, czyli poprzez implementację odpowiedniego interfejsu pozwala na
     * zrobienie czegoś przed i po metodzie testowej. Dlaczego jest to lepsze od @Before i @After - ponieważ raz zaimplementowane,
     * może być używane potem w wielu testach bez dziedziczenia i bez copy-paste
     *
     * Dostępne w JUnit defultowo :
     * --RuleChain (org.junit.rules)
     * --TestWatcher (org.junit.rules)
     * --Verifier (org.junit.rules)
     * --ExternalResource (org.junit.rules)
     * --Stopwatch (org.junit.rules)
     * --Timeout (org.junit.rules)
     * --DisableOnDebug (org.junit.rules)
     * --ExpectedException (org.junit.rules)
     * <p>
     *
     *
     * Można tworzyć własne (wystarczy rozszerzyć klasę TestRule - https://medium.com/@vanniktech/writing-your-own-junit-rule-3df41997b10c)
     * Przykłady użycia @Rule:
     */

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void testRule() throws IOException {
        File newFolder = tempFolder.newFolder("Temp Folder");
        assertTrue(newFolder.exists());
    }


}
