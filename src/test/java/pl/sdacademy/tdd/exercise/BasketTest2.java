package pl.sdacademy.tdd.exercise;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Uruchom test:
 * Ctrl+ Shift+ F10
 *
 * Zwróć uwage na
 *
 * @RunWith(JUnitParamsRunner.class)
 * http://junit.sourceforge.net/javadoc/org/junit/runner/RunWith.html
 *
 */
@RunWith(JUnitParamsRunner.class)
public class BasketTest2 {


    @Before
    public void init() {
        System.out.println("INIT TEST");
    }


    @Test
    @Parameters({"IPHONE", "XBOX", "VODKA"})
    public void hasIphoneProduct(String iphone) {
        //GIVEN
        Basket basket = new Basket(Arrays.asList(new Item(iphone, 5, 55)));

        //WHEN
        boolean hasP1 = basket.has(iphone);

        //THEN
        assertTrue(hasP1);
    }

    @Test
    @Parameters({"IPHONE,XBOX,VODKA"})
    public void hasIphoneOnlyCommaSeparated(String iphone,String xbox, String vodka) {
        //GIVEN
        Basket basket = new Basket(Arrays.asList(new Item(iphone, 5, 55)));

        //WHEN
        boolean hasIphone = basket.has(iphone);
        boolean hasXbox = basket.has(xbox);
        boolean hasVodka = basket.has(vodka);

        //THEN
        assertTrue(hasIphone);
        assertFalse(hasXbox);
        assertFalse(hasVodka);
    }


    @Test
    @Parameters({"IPHONE|XBOX|VODKA"})
    public void hasIphoneOnlyPipeSeparated(String iphone,String xbox, String vodka) {
        //GIVEN
        Basket basket = new Basket(Arrays.asList(new Item(iphone, 5, 55)));

        //WHEN
        boolean hasIphone = basket.has(iphone);
        boolean hasXbox = basket.has(xbox);
        boolean hasVodka = basket.has(vodka);

        //THEN
        assertTrue(hasIphone);
        assertFalse(hasXbox);
        assertFalse(hasVodka);
    }

    @Test
    @Parameters(method = "provider")
    public void paramsInNamedMethod(String p1, Integer p2) {

        System.out.println("Parametry dostarczone z metody provider");
        System.out.println(p1);
        System.out.println(p2);

    }
    private Object provider() {
        return new Object[]{"AAA", 1};
    }


}
