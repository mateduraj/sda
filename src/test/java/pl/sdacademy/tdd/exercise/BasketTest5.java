package pl.sdacademy.tdd.exercise;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertEquals;
import static pl.sdacademy.tdd.exercise.DiscountApplier.IPHONE_PRODUCT;
import static pl.sdacademy.tdd.exercise.DiscountApplier.MACBOOK_PRODUCT;

/**
 * https://www.baeldung.com/mockito-behavior
 */
@RunWith(MockitoJUnitRunner.class)
public class BasketTest5 {

    private DiscountApplier applier;

    @Mock
    private Basket basket1;

    private Basket basket2;

    @Before
    public void init() {
        applier = new DiscountApplier();
    }


    @Test
    public void mockListVerifyAddingElement() {
        List<String> mockList = Mockito.mock(ArrayList.class);

        mockList.add("one");
        mockList.add("two");

        Mockito.verify(mockList).add("one");
        Mockito.verify(mockList).add("two");

        Mockito.doReturn(100).when(mockList).size();
        assertEquals(100, mockList.size());
    }


    @Test(expected = IllegalStateException.class)
    public void throwExceptionOnAddAttempt() {
        List listMock = Mockito.mock(List.class);
        Mockito.when(listMock.add(Mockito.anyString())).thenThrow(IllegalStateException.class);

        listMock.add("why leo ?");
    }

    @Test
    public void basketOnlyContainsXboxThenProductDiscountApplied() {
        //Utwórz mocka
        basket2 = Mockito.mock(Basket.class);
        double basketAmount = 2.0;

        //Jeśli zostanie wywołana metoda has z klasy basket z argumentem iphone, nasz mock zwróci true
        //Pamietaj , że jako argument możesz użyć matchera którego warunek będzie prawdziwy dla wszystkich argumentów tj. Mockito.any()
        Mockito.when(basket2.has(DiscountApplier.XBOX_PRODUCT)).thenReturn(true);

        //Jeśli zostanie wywołana metoda basket2.getAmount, to zwróc 2.0
        Mockito.when(basket2.getAmount()).thenReturn(basketAmount);

        //Wywołaj metodę
        applier.apply(basket2);

        //Przechwyć parametr przekazany do wywołania metody substract , naszego mocka basket2
        ArgumentCaptor<Double> discountCaptor = ArgumentCaptor.forClass(Double.class);
        Mockito.verify(basket2).subtract(discountCaptor.capture());

        //Pobierz przechwyconą wartość
        Double value = discountCaptor.getValue();
        assertThat(value).isEqualTo(basketAmount * 0.7);

    }


    @Test
    public void testWhenMacbookProduct() {
        Basket basket = Mockito.mock(Basket.class);

        Mockito.when(basket.has(MACBOOK_PRODUCT)).thenReturn(true);
        Mockito.when(basket.has(IPHONE_PRODUCT)).thenReturn(true);

        Mockito.when(basket.getAmount()).thenReturn(20.00);

        DiscountApplier applier = new DiscountApplier();

        applier.apply(basket);

        ArgumentCaptor<Double> discountCaptor = ArgumentCaptor.forClass(Double.class);
        Mockito.verify(basket).subtract(discountCaptor.capture());

        Double value = discountCaptor.getValue();
        assertThat(value).isEqualTo(basket.getAmount() * 0.15);

    }


}
