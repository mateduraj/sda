package pl.sdacademy.tdd.exercise;


import org.junit.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.*;

public class BasketTest4 {


    /**
     * Zbiorczy test
     * <p>
     * Jeśli potrzebujesz własnych asercji:
     * http://joel-costigliola.github.io/assertj/assertj-core-custom-assertions.html
     */
    @Test
    public void assertJUsuageTest() {
        Beer zywiec = new Beer("Żywiec", 3.25);
        Beer tyskie = new Beer("Tyskie", 2.25);

        //każdy chyba pił :)
        assertThat(zywiec).isNotEqualTo(tyskie);

        Beer vip = new Beer("Vip", 5.25);
        List<Beer> beers = Arrays.asList(zywiec, tyskie, vip);

        // array i sprawdzenie czy lista zawiera elementy
        assertThat(beers).contains(vip);

        assertThat(beers)
                //Bo wykształcenie to nie piwo, nie musi być pełne!
                .isNotEmpty()
                .contains(vip)
                .doesNotContainNull()
                .containsSequence(zywiec, tyskie);


        //porównywanie stringów
        assertThat(zywiec.getName())
                .isNotEqualTo('Ż')
                .inUnicode()
                .startsWith("Ż");

        //porównywanie pojedynczych znaków
        assertThat('d')
                .isNotEqualTo('a')
                .inUnicode()
                .isGreaterThanOrEqualTo('b')
                .isLowerCase();


        //double /float - porównywanie
        assertThat(tyskie.getPrice())
                .isEqualTo(2, withPrecision(1d));


        //Hash map
        Map<Integer, String> hashMap = new HashMap<>();
        hashMap.put(2, "a");

        assertThat(hashMap)
                .isNotEmpty()
                .containsKey(2)
                .doesNotContainKeys(10)
                .contains(entry(2, "a"));


        //wyjatki
        Exception ex = new RuntimeException("Error during processing file");
        assertThat(ex).hasNoCause().hasMessageEndingWith("file");


        //Asercje można opisywac - w momencie gdy przestanie działa , zostanie wypisany exception
        // bardzo pomocne do szybkiej weryfkacji i zlokalizowania klasy/modułu gdzie moglismy coś popsuć :)
        assertThat(zywiec.getPrice())
                .as("%s's age should be equal to 100", tyskie.getName())
                .isEqualTo(2, withPrecision(1d));

    }


    private class Beer {
        private String name;
        private double price;

        public Beer(String beerName, double price) {
            this.name = beerName;
            this.price = price;
        }


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Beer beer = (Beer) o;
            return Double.compare(beer.price, price) == 0 &&
                    Objects.equals(name, beer.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, price);
        }

    }


}
