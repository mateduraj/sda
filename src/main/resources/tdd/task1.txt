Zapoznaj się z strukturą klas w __todo__


W pierwszej kolejności zweryfikuj czy w pom.xml znajduje się zależnośc do bibloteki junit:


<!-- https://mvnrepository.com/artifact/junit/junit -->
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
    <scope>test</scope>
</dependency>

W klasie BasketTest1.java


a)napraw test hasIphoneProduct()
b)dopisz test dla metod (klasa Basket). Ilość zależy od Ciebie - dobieraj tak wartości, ażeby pokryć jak najwięcej warunków brzegowych. Minimum to 3 testy / metode

     * subtract
     * getAmount
     * qtyOfItems
     * has

UWAGA- 1 Zanim zaczniesz pisać testy, warto zapoznać się z klasą org.junit.Assert
UWAGA- 2 Możesz badać pokrycie testu(test coverage), w środowisku Intelij wystarczy
UWAGA- 3 https://vitalflux.com/7-popular-unit-test-naming-conventions/ - jeśli potrzebujesz wsparcia odnosnie nazewnictwa testów,
technik jest wiele - wybór należy do Ciebie , ja preferuje regułe numer 4 ;)



Pamietaj! , po zakończeniu o commicie do swojego repo - TASK1_SOLUTION
