package pl.sdacademy.tdd.exercise;

public class DiscountApplier {

    public static final String MACBOOK_PRODUCT = "MACBOOK";
    public static final String IPHONE_PRODUCT = "IPHONE";
    public static final String NOTEBOOK_PRODUCT = "NOTEBOOK";
    public static final String WINDOWS_PHONE_PRODUCT = "WINDOWS PHONE";
    public static final String XBOX_PRODUCT = "XBOX";

    public void apply(Basket basket) {
        boolean ok = discountPerProduct(basket);
        if(!ok) discountPerAmount(basket);
    }

    private boolean discountPerProduct(Basket basket) {
        if(basket.has(MACBOOK_PRODUCT) && basket.has(IPHONE_PRODUCT)) {
            basket.subtract(basket.getAmount() * 0.15);
            return true;
        }

        if(basket.has(NOTEBOOK_PRODUCT) && basket.has(WINDOWS_PHONE_PRODUCT)) {
            basket.subtract(basket.getAmount() * 0.12);
            return true;
        }

        if(basket.has(XBOX_PRODUCT)) {
            basket.subtract(basket.getAmount() * 0.7);
            return true;
        }

        return false;
    }

    private void discountPerAmount(Basket basket) {

        if(basket.getAmount()<=1000 && basket.qtyOfItems() <= 2) {
            basket.subtract(basket.getAmount() * 0.02);
        }

        else if(basket.getAmount() > 3000 && basket.qtyOfItems() < 5 && basket.qtyOfItems() > 2) {
            basket.subtract(basket.getAmount() * 0.05);
        }

        else if(basket.getAmount() > 3000 && basket.qtyOfItems() >= 5) {
            basket.subtract(basket.getAmount() * 0.06);
        }
    }

}
