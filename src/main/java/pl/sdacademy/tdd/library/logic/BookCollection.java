package pl.sdacademy.tdd.library.logic;

import pl.sdacademy.tdd.library.domain.Book;

public interface BookCollection {

    Book findBookByTitle(String title);

    void printAllBooks();

    void addBook(Book book);


}
