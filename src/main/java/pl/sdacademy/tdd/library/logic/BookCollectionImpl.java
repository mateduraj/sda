package pl.sdacademy.tdd.library.logic;

import pl.sdacademy.tdd.library.domain.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookCollectionImpl implements BookCollection {

    public static final String BOOK_NOT_FOUND = "Nie znaleziono ksiązki";
    private final List<Book> books = new ArrayList<>();

    @Override
    public Book findBookByTitle(String title) {
        Optional<Book> book = books
                .stream()
                .filter(b -> b.getTitle().equals(title)).findFirst();

        return book.orElseThrow(() -> new RuntimeException(BOOK_NOT_FOUND));
    }

    @Override
    public void printAllBooks() {
        books.stream().forEach(System.out::println);
    }

    @Override
    public void addBook(Book book) {

        if(book == null){
            throw new RuntimeException("Book cannot be null");
        }

        books.add(book);
    }

}
