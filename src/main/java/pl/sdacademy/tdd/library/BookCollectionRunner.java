package pl.sdacademy.tdd.library;

import pl.sdacademy.tdd.library.logic.BookCollection;
import pl.sdacademy.tdd.library.logic.BookCollectionImpl;

public class BookCollectionRunner {

    public static void main(String args[]) {

        BookCollection bookCollection = new BookCollectionImpl();

        bookCollection.printAllBooks();
        System.out.println(bookCollection.findBookByTitle("Title1"));

    }

}
