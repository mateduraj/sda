package pl.sdacademy.tdd.library.domain;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import pl.sdacademy.tdd.library.domain.converter.PriceConverter;

public class Book extends AbstractEntity {

    @CsvBindByName(required = true)
    private int id;

    @CsvBindByName(required = true)
    private String title;

    @CsvBindByName(required = true)
    private String author;

    @CsvBindByName(required = true)
    private String format;

    @CsvBindByName
    @CsvCustomBindByName(column = "price", converter = PriceConverter.class)
    private Price price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }
}
