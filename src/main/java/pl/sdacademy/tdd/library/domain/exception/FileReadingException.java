package pl.sdacademy.tdd.library.domain.exception;

public class FileReadingException extends RuntimeException {

    public FileReadingException(Throwable throwable) {
        super(String.format("Wystąpił problem podczas odczytu plik %s", throwable.getMessage()));
    }
}
