package pl.sdacademy.tdd.library.domain.dictionary;

public enum Gender {
    M, F
}
