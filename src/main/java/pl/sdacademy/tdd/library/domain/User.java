package pl.sdacademy.tdd.library.domain;


import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import pl.sdacademy.tdd.library.domain.converter.GenderConverter;
import pl.sdacademy.tdd.library.domain.dictionary.Gender;

public class User extends AbstractEntity {

    @CsvBindByName
    private String firstName;

    @CsvBindByName
    private String surname;

    @CsvBindByName
    private String title;

    @CsvBindByName
    private String address1;

    @CsvBindByName
    private String address2;

    @CsvBindByName
    private String town;

    @CsvBindByName
    private String county;

    @CsvBindByName
    private String postCode;

    @CsvBindByName
    private String subsribed;

    @CsvBindByName()
    @CsvCustomBindByName(column = "gender", converter = GenderConverter.class)
    private Gender gender;

    @CsvBindByName
    private String dateOfBirth;

    @CsvBindByName
    private String login;

    @CsvBindByName
    private String password;


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getSubsribed() {
        return subsribed;
    }

    public void setSubsribed(String subsribed) {
        this.subsribed = subsribed;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
