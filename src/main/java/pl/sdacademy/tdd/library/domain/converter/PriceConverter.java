package pl.sdacademy.tdd.library.domain.converter;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import pl.sdacademy.tdd.library.domain.Price;

public class PriceConverter extends AbstractBeanField<Price> {

    @Override
    protected Price convert(String s) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        return new Price();
    }
}
