package pl.sdacademy.tdd.library.domain;

import com.opencsv.bean.CsvBindByName;

public abstract class AbstractEntity {

    @CsvBindByName(required = true)
    private int id;

    public int getId() {
        return id;
    }
}
