package pl.sdacademy.tdd.library.console.runner.menu.logic;

import pl.sdacademy.tdd.library.console.runner.menu.context.ConsoleContext;
import pl.sdacademy.tdd.library.console.runner.menu.factory.DelegateFactory;
import pl.sdacademy.tdd.library.console.runner.menu.model.MenuCommandValue;

public class ConsoleHandler {

    private final ConsoleContext context;

    public ConsoleHandler(ConsoleContext context) {
        this.context = context;
    }

    /**
     *
     * @param userCommand - wartość przekazane przez użtkownika z konsoli
     *
     *                    Design pattern - template method
     */
    public void handle(String userCommand) {
        MenuCommandValue command = MenuCommandValue.translate(userCommand);
        context.setSelectedCommand(command);
        ConsoleDelegate consoleDelegate = DelegateFactory.fromCommand(context);
        consoleDelegate.delegate();
    }

    public boolean isLoggedIn() {
        return context.getUserSession().getLogged();
    }

}
