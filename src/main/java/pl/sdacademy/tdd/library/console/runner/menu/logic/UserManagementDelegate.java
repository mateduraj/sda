package pl.sdacademy.tdd.library.console.runner.menu.logic;

import pl.sdacademy.tdd.library.console.runner.menu.context.ConsoleContext;
import pl.sdacademy.tdd.library.console.runner.menu.model.UserManagementMenuOptionResult;
import pl.sdacademy.tdd.library.console.runner.menu.options.MenuOptionCommand;
import pl.sdacademy.tdd.library.console.runner.menu.options.UserManagementCommand;

public class UserManagementDelegate extends ConsoleDelegate<UserManagementMenuOptionResult> {


    public UserManagementDelegate(ConsoleContext consoleContext) {
        super(consoleContext);
    }

    @Override
    protected void afterExecuteCommand(UserManagementMenuOptionResult result) {
        int action = result.getAction();
        System.out.println("User managment action " + action);
    }

    @Override
    protected MenuOptionCommand<UserManagementMenuOptionResult> getCommand() {
        return new UserManagementCommand();
    }

}
