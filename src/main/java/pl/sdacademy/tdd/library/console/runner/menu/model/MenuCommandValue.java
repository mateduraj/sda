package pl.sdacademy.tdd.library.console.runner.menu.model;

public enum MenuCommandValue {

    LOGIN(0),
    USER_MANAGEMENT(1),
    LOGOUT(99);

    private int value;

    MenuCommandValue(int value) {
        this.value = value;
    }

    public static MenuCommandValue translate(String value) {

        for (MenuCommandValue command : MenuCommandValue.values()) {
            if (command.value == Integer.valueOf(value)) {
                return command;
            }

        }

        throw new RuntimeException(String.format("Unrecognized command %s", value));

    }

}
