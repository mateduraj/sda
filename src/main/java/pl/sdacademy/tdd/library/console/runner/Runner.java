package pl.sdacademy.tdd.library.console.runner;

import java.io.IOException;

public interface Runner {

    void execute(String[] args) throws IOException;

}
