package pl.sdacademy.tdd.library.console.runner.menu.factory;


import pl.sdacademy.tdd.library.console.runner.menu.context.ConsoleContext;
import pl.sdacademy.tdd.library.console.runner.menu.logic.ConsoleDelegate;
import pl.sdacademy.tdd.library.console.runner.menu.logic.LoginDelegate;
import pl.sdacademy.tdd.library.console.runner.menu.logic.LogoutDelegate;
import pl.sdacademy.tdd.library.console.runner.menu.logic.UserManagementDelegate;

//DESIGN PATTERN FACTORY METHOD
public class DelegateFactory {

    public static ConsoleDelegate fromCommand(ConsoleContext consoleContext) {
        switch (consoleContext.getSelectedCommand()) {

            case LOGIN:
                return new LoginDelegate(consoleContext);
            case USER_MANAGEMENT:
                return new UserManagementDelegate(consoleContext);
            case LOGOUT:
                return new LogoutDelegate(consoleContext);
        }

        throw new RuntimeException(String.format("Unrecognized command", consoleContext.getSelectedCommand()));
    }

}
