package pl.sdacademy.tdd.library.console.runner.menu.model;

public class LoginOptionResult extends MenuOptionResult {

    private String login;
    private String password;

    public LoginOptionResult(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
