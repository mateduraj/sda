package pl.sdacademy.tdd.library.console.runner.menu.options;


import pl.sdacademy.tdd.library.console.runner.menu.model.MenuOptionResult;

public abstract class MenuOptionCommand<R extends MenuOptionResult> {


    public abstract R execute();

}
