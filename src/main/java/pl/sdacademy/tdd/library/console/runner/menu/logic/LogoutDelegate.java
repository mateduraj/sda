package pl.sdacademy.tdd.library.console.runner.menu.logic;

import pl.sdacademy.tdd.library.console.runner.menu.context.ConsoleContext;
import pl.sdacademy.tdd.library.console.runner.menu.model.MenuOptionResult;
import pl.sdacademy.tdd.library.console.runner.menu.options.MenuOptionCommand;

public class LogoutDelegate extends ConsoleDelegate {

    public LogoutDelegate(ConsoleContext consoleContext) {
        super(consoleContext);
    }

    @Override
    protected void afterExecuteCommand(MenuOptionResult result) {

    }

    @Override
    protected MenuOptionCommand getCommand() {
        return null;
    }

}
