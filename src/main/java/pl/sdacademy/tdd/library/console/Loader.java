package pl.sdacademy.tdd.library.console;


import pl.sdacademy.tdd.library.console.runner.ConsoleRunner;

import java.io.IOException;

public class Loader {

    public static void main(String args[]) throws IOException {
        new ConsoleRunner().execute(args);
    }

}
