package pl.sdacademy.tdd.library.console.runner;

import pl.sdacademy.tdd.library.console.runner.menu.context.ConsoleContext;
import pl.sdacademy.tdd.library.console.runner.menu.logic.ConsoleHandler;

import java.io.IOException;
import java.util.Scanner;

public class ConsoleRunner implements Runner {


    @Override
    public void execute(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        ConsoleContext menuContext = ConsoleContext.instance();
        ConsoleHandler handler = new ConsoleHandler(menuContext);

        while (true) {
            try {

                if(!handler.isLoggedIn()){
                    handler.handle("0");
                }else{
                    //Czy użytkownik niezalogowany
                    //Użytkownik zalogowany , wyświetl opcje
                    System.out.println("Welcome in the book shop, select option");
                    System.out.println(prepareWelcomeMessage());
                    String command = sc.next();
                    System.out.println("You choose: " + command);
                    handler.handle(command);
                }


            } catch (RuntimeException ex) {
                System.out.println(String.format("Exception occured, exception code :  %s", ex.getMessage()));
            }

        }

    }

    /*   
                   Opcje:

                       Logowanie
                       - Podaj nazwę uzytkownika
                       - Podan hasło
                       0. Użytkownicy
                           - wyświel użytkowników
                           - nadaj uprawnienia
                           - usun uprawnienia
                       1. Ksiązki
                           -wyświetl wszystkie
                           -wyświelt promocyjne
                           -wyświetl w przedsprzedaży
                           -wyświetl w przygotowaniu
                           -dodaj nową ksiązke (tylko administrator)

                       2.  Twoje zamówienia
                       3.  Twój koszyk
                       5.  Twoje ulubione
                       6.  Twoja przechowalnia
                       7.  Historia płatności
                       8.  Faktury VAT
                       9.  Dodaj metodę płatności
                       10. Edytuj profil
                       11. Wyloguj
                       99. Return
           */
    private static String prepareWelcomeMessage() {

        return "TODO - Uzupełnij listę";
    }

}
