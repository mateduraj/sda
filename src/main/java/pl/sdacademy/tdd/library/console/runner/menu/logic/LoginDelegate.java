package pl.sdacademy.tdd.library.console.runner.menu.logic;

import pl.sdacademy.tdd.library.console.runner.menu.context.ConsoleContext;
import pl.sdacademy.tdd.library.console.runner.menu.model.LoginOptionResult;
import pl.sdacademy.tdd.library.console.runner.menu.options.UserLoginCommand;

public class LoginDelegate extends ConsoleDelegate<LoginOptionResult> {

    public LoginDelegate(ConsoleContext consoleContext) {
        super(consoleContext);

    }

    @Override
    protected void afterExecuteCommand(LoginOptionResult result) {
        //TODO AuthorityService.login
        consoleContext.login();
    }


    @Override
    protected UserLoginCommand getCommand() {
        return new UserLoginCommand();
    }

}
