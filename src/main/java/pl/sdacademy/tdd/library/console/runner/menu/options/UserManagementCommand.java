package pl.sdacademy.tdd.library.console.runner.menu.options;



import pl.sdacademy.tdd.library.console.runner.menu.model.UserManagementMenuOptionResult;

import java.util.Scanner;

public class UserManagementCommand extends MenuOptionCommand<UserManagementMenuOptionResult> {

    @Override
    public UserManagementMenuOptionResult execute() {
        Scanner sc = new Scanner(System.in);
        System.out.println(createMessage());
        String action = sc.next();
        return new UserManagementMenuOptionResult(Integer.valueOf(action));
    }

    private String createMessage() {
        return new StringBuilder("Zarządzanie uzytkownikami")
                .append("1 - Wyświetl wszystkich")
                .append("2 - nadaj uprawnienia")
                .append("3 - wyświetl aktualne uprawnienia użytkownika ")
                .toString();
    }
}
