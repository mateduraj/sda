package pl.sdacademy.tdd.library.console.runner.menu.context;

import pl.sdacademy.tdd.library.console.runner.menu.model.MenuCommandValue;

public class ConsoleContext {

    private MenuCommandValue selectedCommand;

    private UserSession userSession;

    public static ConsoleContext instance() {
        return new ConsoleContext();
    }

    private ConsoleContext() {
        selectedCommand = MenuCommandValue.LOGIN;
        userSession = new UserSession();
    }

    public MenuCommandValue getSelectedCommand() {
        return selectedCommand;
    }

    public void setSelectedCommand(MenuCommandValue selectedCommand) {
        this.selectedCommand = selectedCommand;
    }

    public UserSession getUserSession() {
        return userSession;
    }

    public void login() {
        userSession.login();
    }
}
