package pl.sdacademy.tdd.library.console.runner.menu.logic;

import pl.sdacademy.tdd.library.console.runner.menu.context.ConsoleContext;
import pl.sdacademy.tdd.library.console.runner.menu.model.MenuOptionResult;
import pl.sdacademy.tdd.library.console.runner.menu.options.MenuOptionCommand;

public abstract class ConsoleDelegate<T extends MenuOptionResult> {

    protected final ConsoleContext consoleContext;

    public ConsoleDelegate(ConsoleContext consoleContext) {
        this.consoleContext = consoleContext;
    }

    //TEMPLATE METHOD DESIGN PATTERN
    public final void delegate() {
        beforeCommand();
        T executeResult = getCommand().execute();
        afterExecuteCommand(executeResult);
    }

    protected void beforeCommand() {
        //Do nothing by default
    }

    protected abstract void afterExecuteCommand(T result);

    //DESIGN PATTERN STRATEGY
    protected abstract MenuOptionCommand<T> getCommand();

}
