package pl.sdacademy.tdd.library.console.runner.menu.options;

import pl.sdacademy.tdd.library.console.runner.menu.model.LoginOptionResult;

import java.util.Scanner;

public class UserLoginCommand extends MenuOptionCommand<LoginOptionResult> {


    @Override
    public LoginOptionResult execute() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj login użytkownika:");
        String login = sc.next();
        System.out.println("Podaj hasło użytkownika:");
        String password = sc.next();
        return new LoginOptionResult(login,password);
    }
}
