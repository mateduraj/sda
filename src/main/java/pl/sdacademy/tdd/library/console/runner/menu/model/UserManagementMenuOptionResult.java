package pl.sdacademy.tdd.library.console.runner.menu.model;

public class UserManagementMenuOptionResult extends MenuOptionResult{

    private int action;

    public UserManagementMenuOptionResult(int action) {
        this.action = action;
    }

    public int getAction() {
        return action;
    }
}
