package pl.sdacademy.tdd.library.console.runner.menu.context;

public class UserSession {

    private Boolean isLogged= false;

    public UserSession() {
    }

    public void login() {
        isLogged = true;
    }

    public Boolean getLogged() {
        return isLogged;
    }
}
