package pl.sdacademy.tdd.library.repository.reader;

import com.opencsv.CSVReader;
import pl.sdacademy.tdd.library.domain.AbstractEntity;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * https://www.callicoder.com/java-read-write-csv-file-opencsv/
 */
public class CsvFileReader<T extends AbstractEntity> extends FileReader<T> {

    public CsvFileReader(String inputFilePath) {
        super(inputFilePath);
    }

    public List<T> readFile(Class<T> clazz) {

        List<T> result = new ArrayList<>();

        try {
            InputStream resourceAsStream = getClass().getResourceAsStream(inputFilePath);
            Reader reader = new InputStreamReader(resourceAsStream);
            CSVReader csvReader = new CSVReader(reader);

            // Reading All Records at once into a List<String[]>
            List<String[]> records = csvReader.readAll();
            for (String[] record : records) {
                System.out.println("Reader1 : " + record[0]);
                System.out.println("Reader2: " + record[1]);
                System.out.println("Reader3 : " + record[2]);
                System.out.println("Reader4 : " + record[3]);
                System.out.println("---------------------------");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

}
