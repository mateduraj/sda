package pl.sdacademy.tdd.library.repository.reader;

import pl.sdacademy.tdd.library.domain.AbstractEntity;

import java.util.List;
import java.util.function.Function;

/**
 * https://www.callicoder.com/java-read-write-csv-file-opencsv/
 */
public abstract class FileReader<T extends AbstractEntity> {

    protected final String inputFilePath;

    public FileReader(String inputFilePath) {
        this.inputFilePath = inputFilePath;
    }

    public abstract List<T> readFile(Class<T> clazz);

}
