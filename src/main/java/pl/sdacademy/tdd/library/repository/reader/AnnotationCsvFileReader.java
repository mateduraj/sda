package pl.sdacademy.tdd.library.repository.reader;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import pl.sdacademy.tdd.library.domain.AbstractEntity;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.function.Function;

/**
 * https://www.callicoder.com/java-read-write-csv-file-opencsv/
 */
public class AnnotationCsvFileReader<T extends AbstractEntity> extends FileReader<T> {

    public AnnotationCsvFileReader(String inputFilePath) {
        super(inputFilePath);
    }

    public List<T> readFile(Class<T> clazz) {

        InputStream resourceAsStream = getClass().getResourceAsStream(inputFilePath);
        Reader reader = new InputStreamReader(resourceAsStream);

        CsvToBean<T> csvToBean = new CsvToBeanBuilder(reader)
                .withType(clazz)
                .withIgnoreLeadingWhiteSpace(true)
                .build();

        return csvToBean.parse();
    }

}
